#include <iostream>
#include "Zoo.h"

unsigned Zoo::quantity = 0;

Zoo::Zoo() {
    counter();
}

Zoo::Zoo(string _animal, string _name,  bool _sex, double _price) {
    setAnimal(_animal);
    setName(_name);
    setSex(_sex);
    setPrice(_price);
    counter();
}

Zoo::Zoo(const Zoo &object) {
    animal = object.animal;
    name = object.name;
    sex = object.sex;
    price = object.price;
    quantity = object.quantity;
}

Zoo::~Zoo() {}

unsigned Zoo::changeParams(string _name) {
    this->setName(_name);
    return this->getQuantity();
}

double Zoo::changeParams(double _price) {
    this->setPrice(_price);
    return this->getPrice();
}