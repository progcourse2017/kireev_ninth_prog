cmake_minimum_required(VERSION 3.8)
project(project9)

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES main.cpp Zoo.cpp Zoo.h)
add_executable(project9 ${SOURCE_FILES})