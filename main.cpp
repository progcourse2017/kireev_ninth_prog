#include <iostream>
#include <vector>
#include "Zoo.h"

using namespace std;

Zoo changeParam(Zoo _animal);
Zoo enter(Zoo _animal);
void print(Zoo _animal);

struct data {
    string animale, name;
    double price;
} anim;

int main(int argc, char **argv) {
    Zoo object("Dog", "Tima", true, 1000);
    vector<Zoo> data;
    unsigned choice = 0;

    data.push_back(object);

    while (choice != 4) {
        std::cout << "What do you want to do?\n"
                  << "1) Add a note\n"
                  << "2) Print all notes\n"
                  << "3) Fix params\n"
                  << "4) Quit the program\n"
                  << "Your choice: ";
        std::cin >> choice;
        std::cout << "\n";
        switch (choice) {
            case 1:
                data.push_back(enter(object));
                object.counter();
                cout << object.getQuantity() << endl;
                break;
            case 2:
                for (unsigned count = 0; count < object.getQuantity(); ++count) {
                    cout << count + 1 << ")\n";
                    print(data[count]);
                }
                break;
            case 3:
                for (unsigned count = 0; count < object.getQuantity(); ++count) {
                    cout << count + 1 << ")\n";
                    print(data[count]);
                }

                unsigned changeChoice;
                cout << "\nWhat kind of object do you want to change: ";
                cin >> changeChoice;

                data[changeChoice - 1] = changeParam(data[changeChoice - 1]);
                break;
            case 4:
                std::cout << "Exit...\n";
                system("pause");
                return 0;
            default:
                std::cout << "Invalid input!\n";
        }
    }
}

Zoo enter(Zoo _anim) {
    unsigned valueU = 0;

    cout << "Enter what animal do you want to add: ";
    cin >> anim.animale;
    _anim.setAnimal(anim.animale);

    cout << "Name: ";
    cin >> anim.name;
    _anim.setName(anim.name);

    while (valueU > 2 || valueU < 1) {
        cout << "Sex: \n"
             << "     1) Male\n"
             << "     2) Female\n";
        cin >> valueU;
        switch (valueU) {
            case 1:
                _anim.setSex(1);
                break;
            case 2:
                _anim.setSex(0);
                break;
            default:
                cout << "Error!\n";
        }
    }

    cout << "Price: ";
    cin >> anim.price;
    _anim.setPrice(anim.price);

    cout << "\n";

    return _anim;
}

void print(Zoo _animal) {
    cout << "  Animal: " << _animal.getAnimal() << "\n"
         << "  Name: " << _animal.getName() << "\n"
         << "  Sex: ";
    if (_animal.getSex())
        cout << "male";
    else
        cout << "female";
    cout << "\n"
         << "  Price: $" << _animal.getPrice() << "\n\n";
}

Zoo changeParam(Zoo _animal) {
    cout << "New name: ";
    cin >> anim.name;
    cout << "Is established: " << _animal.changeParams(anim.name) <<endl;

    cout << "New price: ";
    cin >> anim.price;
    cout << "Is established: " << _animal.changeParams(anim.price) <<endl;

    cout << "\n";

    return _animal;
}